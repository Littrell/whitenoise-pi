#!/bin/bash

# NOTE - This only works in Debian  based operating systems
# It was written for a Raspberry pi

# Add cron entry to auto start the application
crontab -u pi crontab

# Check for mpg123 and install it if it doesn't exist
if ! type "mpg123" > /dev/null; then
  sudo apt-get install mpg123 -y
fi

# Check for pulseaudio and install it if it doesn't exist
if ! type "pulseaudio" > /dev/null; then
    sudo apt-get install pulseaudio -y
fi

sudo reboot
