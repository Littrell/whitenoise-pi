This is a super simple (dumb) repository that can be used to set up a Raspberry Pi as a whitenoise generator. It only works on Debian-based operating systems. It doesn't contain any controls of any sort. All it does is setup a cron that plays a whitenoise mp3 on loop. The cron fires off 30 seconds after the system starts up.

This is for my personal use but by all means feel free to use this. Eventually I'll make this less dumb.

# Setup

```bash
# Step 1 - Download this repository to /home/pi/Downloads

# Step 2 - Move run.sh to /home/pi/Downloads (told you this was dumb)

# Step 3 - Move 03-White-Noise-10min.mp3 to /home/pi/Downloads

# Step 4 - Run the following:
./setup.sh

# The system should reboot after the script finishes and you should hear whitenoise playing
```
